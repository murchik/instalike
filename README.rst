#########
InstaLike
#########

Social networks turn people into like-bots. This script attempts to give you
your humanity back by liking everything in your feed. That's what you're doing
anyway, right?

Created with one eye closed. Tested with FF + Greasemonkey and Tampermonkey.
